package com.makemytrip.samplecustomchrome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsCallback;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity implements ServiceConnectionCallback {

    private static final String TAG = "anukalp";
    private static final String TAG_SET_PWA_COOKIE = "pwa-cookie1";
    private static final String TOOLBAR_COLOR = "#3F51B5";
    public static final String TAG_COKKIE_URL = "https://www.makemytrip.com/pwa/cookie2.html";
    private CustomTabsClient mClient;
    private CustomTabsServiceConnection mConnection;
    private String mPackageNameToBind = MMTCustomTabsHelper.STABLE_PACKAGE;
    private CustomTabsSession mCustomTabsSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences mSharedPref = this.getPreferences(Context.MODE_PRIVATE);
        if (mSharedPref.getBoolean(TAG_SET_PWA_COOKIE, true)) {
            bindCustomTabsService();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences mSharedPref = this.getPreferences(Context.MODE_PRIVATE);
        if (!mSharedPref.getBoolean(TAG_SET_PWA_COOKIE, true)) {
            unbindCustomTabsService();
        }
    }

    private static class NavigationCallback extends CustomTabsCallback {

        private final WeakReference<MainActivity> mActivity;

        public NavigationCallback(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void onNavigationEvent(int navigationEvent, Bundle extras) {
            Log.w(TAG, "onNavigationEvent: Code = " + navigationEvent);
            switch (navigationEvent) {
                case TAB_HIDDEN:
                case NAVIGATION_FINISHED:
                    if (null != mActivity.get() && !mActivity.get().isFinishing()) {
                        Context appContext = mActivity.get().getApplicationContext();
                        Intent myIntent = new Intent(appContext, MainActivity.class);
                        myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        SharedPreferences mSharedPref = mActivity.get().getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = mSharedPref.edit();
                        editor.putBoolean(TAG_SET_PWA_COOKIE, false);
                        editor.commit();

                        mActivity.get().unbindCustomTabsService();
                        appContext.startActivity(myIntent);
                    }
                    break;
                default:
                    break;
            }
        }
    }


    private CustomTabsSession getSession() {
        if (mClient == null) {
            mCustomTabsSession = null;
        } else if (mCustomTabsSession == null) {
            mCustomTabsSession = mClient.newSession(new NavigationCallback(this));
        }
        return mCustomTabsSession;
    }

    private void bindCustomTabsService() {
        if (mClient != null) return;
        if (TextUtils.isEmpty(mPackageNameToBind)) {
            mPackageNameToBind = MMTCustomTabsHelper.getPackageNameToUse(this);
            if (mPackageNameToBind == null) return;
        }
        mConnection = new ServiceConnection(this);
        boolean ok = CustomTabsClient.bindCustomTabsService(this, mPackageNameToBind, mConnection);
        Log.w(TAG, "bindCustomTabsService: started = " + ok);
        if (!ok) {
            mConnection = null;
        }
    }

    private void unbindCustomTabsService() {
        if (mConnection == null) return;
        unbindService(mConnection);
        mClient = null;
        mConnection = null;
        Log.w(TAG, "unbindCustomTabsService");
    }

    @Override
    protected void onDestroy() {
        unbindCustomTabsService();
        super.onDestroy();
    }

    @Override
    public void onServiceConnected(CustomTabsClient client) {
        mClient = client;
        boolean success = false;
        if (mClient != null) success = mClient.warmup(0);
        if (success) {
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder(getSession());
            builder.setToolbarColor(Color.parseColor(TOOLBAR_COLOR)).setShowTitle(true);
            CustomTabsIntent customTabsIntent = builder.build();
            MMTCustomTabsHelper.addKeepAliveExtra(this, customTabsIntent.intent);
            customTabsIntent.launchUrl(this, Uri.parse(TAG_COKKIE_URL));
        }
    }

    @Override
    public void onServiceDisconnected() {
        mClient = null;
    }
}
